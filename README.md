# **Auto Compose** #

## **About** ##

Auto Compose is a C++ project that uses the [CFugue Audio API](http://cfugue.sourceforge.net/) to automatically generate musical compositions through the automation of a few basic musical principles. This is a work in progress, but the end goal is to create a GUI application that allows a user to specify certain parameters to control musical output. The project was inspired by an attempt to ease the composing process by providing a sort of template from which to start or gather ideas from. The end result should hopefully help someone who may not be musically inclined to create original music if they need it for a personal project (ex. animators, game devs etc.).

## **Goals** ##
### **Interface** ###
The final interface should allow for the creation of various tracks or layers which have their respective modifiable properties (instrument, volume, rhythm, note progression etc.) that change the output generated. The main interface will specify overall project properties (tempo, key signature, time signature etc.) that pertain to the entire composition as a whole. Here is a simple mock up:

![mock up.png](https://bitbucket.org/repo/kAE9x6/images/3778067701-mock%20up.png)

The musical automation process currently depends on 3 basic principles to produce sounds that are pleasing to the ear:

**Euclidean Rhythm** provides a simple yet effective approach to create traditional musical rhythms found in a variety of musical styles. The implementation of Euclidean Rhythm will be based on Bjorklund's algorithm.

**Melodic Progression** will determine the sequence of notes that will be generated in a track. When writing a new note, auto compose will read previous notes in the sequence to create a progression (maybe using a probability distribution?). In this way, a track will "communicate" with itself to create a series of sounds that flow in an elegant and orderly manner.

**Counterpoint** will dictate how separate tracks overlay to produce harmonious sound. In the same way that melodic progression analyzes previous notes within a single track to write new notes, counterpoint will analyze notes in different tracks to ensure that new notes harmonize well.

## **How to Use** ##
The project is still in it's early stages and uses an early build of CFugue. To access the current build in Visual Studio:

1. Download and install the latest version of [CMake](http://www.cmake.org/download/)
2. Download the project
3. Run the CMake gui application. Specify the location of the project folder and the output build. Generate the build.
4. A folder named "test" should have been created (along with some extra clutter created by CMake). Open the solution testCFugueLib.vcxproj
5. SampleApp.cpp contains the source for the current version of Auto Compose