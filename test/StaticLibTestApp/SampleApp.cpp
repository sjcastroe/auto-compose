/*AUTO COMPOSE by Sergio Castro
Auto Compose is an application (work in progress) used to automatically compose music.
It takes advantage of CFugues high level API to create melodies that can be overlayed
to form complete compositions.

The three primary mechanisms powering music automation are:
1. Euclidean Rhythm - Musical rhythms will be produced using bjorklund's algorithm for Euclidean Rhythm
2. Melodic Progression - "Smart" melodies will be created through a composition system that looks at previous notes when
						adding a new note to the melody
3. Counterpoint - This will be the means by which the different layers or tracks will communicate to create harmonies
*/
#include "stdafx.h"
#include "stdlib.h"
#include <iostream>
#include <string>

#include "CFugueLib.h"

class Note
{
public:
	Note() {}
	Note(char* r, char* o, char* c, char* d, char* v) : root(r), octave(o), chord(c), duration(d) {}
	std::string get() const
	{
		return root + octave + chord + duration;
	}
	void set_root(const char* r)
	{
		root = r;
	}
	void set_octave(char* o)
	{
		octave = o;
	}
	void set_chord(char* c)
	{
		chord = c;
	}
	void set_duration(char* d)
	{
		duration = d;
	}
private:
	std::string root;
	std::string octave;
	std::string chord;
	std::string duration;
};

class Measure
{
	Measure() {}
	Measure(char* m) : measure(m) {}
	void write(Note new_note)
	{
		measure = measure + " " + new_note.get();
	}
private:
	std::string measure;
};

class Composer
{
public:
	Composer() {}
	std::string compose(const char* key, const char* tempo, std::string rhythm)
	{
		add_note(rhythm);
		for (int i = 0; i < 2; i++)
			song += song;
		song = "K[" + std::string(key) + "] T[" + std::string(tempo) + "] I[FLUTE]" + song;
		return song;
	}
	void add_note(std::string rhythm)
	{
		for (int i = 0; i < rhythm.size(); i++)
		{
			Note note;
			if (rhythm[i] == '1')
			{
				note.set_root(choose_root().c_str());
				song += " " + note.get() + "/" + std::to_string(0.0625 * 2);
			}
			else
				song = song + " R/" + std::to_string(0.0625 * 2);
		}
		
	}
	std::string choose_root()
	{
		int root = 65 + rand() % (71 - 65 + 1);
		std::string root_str;
		root_str = (char)root;
		return root_str;
	}
	std::string choose_chord()
	{

	}
	std::string song;
};

//Each iteration is a process of pairing X and Y strings and the remainder from the pairings
std::string EuclideanRhythm(int beats, int steps)
{
	if (beats > steps)
		beats = steps;

	//Start with simple pairings (X = 1, Y = 0)
	std::string x = "1";
	int x_amount = beats;

	std::string y = "0";
	int y_amount = steps - beats;

	do
	{
		//Placeholder variables
		int x_temp = x_amount;
		int y_temp = y_amount;
		std::string y_copy = y;

		if (x_temp >= y_temp)
		{
			x_amount = y_temp;
			y_amount = x_temp - y_temp;
			y = x;
		}
		else
		{
			x_amount = x_temp;
			y_amount = y_temp - x_temp;
		}
		x = x + y_copy;
	} while (x_amount > 1 && y_amount > 1);

	std::string rhythm;
	for (int i = 1; i <= x_amount; i++)
		rhythm += x;
	for (int i = 1; i <= y_amount; i++)
		rhythm += y;
	return rhythm;
}



int main()
{
	srand(time(0));
	Composer composer;
	Composer composer2;
	std::string song1 = composer.compose("DMAJ", "180", EuclideanRhythm(5, 8));//set Ecuclidean rhythm to odd and even intervals
	std::string song2 = composer2.compose("DMAJ", "180", EuclideanRhythm(6, 8));
	std::cout << song1 << std::endl << song2 << std::endl;
	std::string song = song1 + " V1 " + song2;
	CFugue::Player player;
	for (;;)
		player.Play(song.c_str());
	return 0;
}


